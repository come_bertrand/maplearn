# -*- coding: utf-8 -*-
"""
Mapping Learning : examples

This script is a good way to discover Mapping Learning possibilities. Using
configuration files available in "examples" subfolder and datasets included in
"datasets" sub-folder, you will see what can do this application.

Example:

        * Asks the user to choose which examples(s) he wants to test::

            $ python run_example.py

        * Execute 3rd example (CLI way)::

            $ python run_example.py 3

        * Launch every examples::

            $ python run_example.py all

"""
from __future__ import print_function
import os
import sys
import subprocess
import inspect

# To remove is usefullness confirmed
#if __name__ == '__main__' and __package__ is None:
#    os.sys.path.append(
#        os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# application path
dir_app = os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe())))
if __name__ == '__main__' and __package__ is None:
    os.sys.path.append(dir_app)

# Description of available examples
EX = (u"""
      Classification supervisée à partir d'un shapefile

      * Les échantillons sont chargés à partir de la table attributaire du
      fichier 'echantillon.shp', avec la colonne 'ECH' qui contient les
      libellés des classes.
      * les données à classer sont dans un second fichier ('data.shp')

      3 algorithmes de classification sont comparés :
          - un arbre de décision simple (tree)
          - k nearest neighbour (knn)
          - Linear Discriminant analysis (lda)

      Le résultat est écrit dans un nouveau shapefile 'sortie.shp'
      """,
      u"""
      Lecture d'un csv avec un dictionnaire de codes

      Les échantillons sont lus à partir d'un fichier csv, avec la colonne
      'Id_classes' qui indique les numéros de classes. Ces identifiants sont
      associés à des libellés grâce à un dictionnaire écrit dans le code

      Lors du pretraitement, les meilleures features sont sélections

      La classification se fait par SVM, avec une recherche des paramètres
      optimaux
      """,
      u"""
      Jeu de données test

      Cet exemple utilise un des deux jeux de données test reconnus en
      traitement du signal : iris et digits

      La separabilité des échantillons est analysée en prétraitement

      Les algorithmes de classification lda, knn et svm sont comparés, après
      avoir recherchés les meilleurs paramètres possibles
      """,
      u"""
      Test des nouveaux algorithmes de classification
      """,
      u"""
      Classification non supervisée à partir d'un shapefile
      """,
      u"""
      Classification non supervisée d'une image satellite
      """,
      u"""
      Classification supervisée d'une image satellite
      """)


def run_example(number, path):
    """
    Run one of available examples in "examples" folder

    Args:
        * number (str) : identifies the example to run
        * path (str) : path to run.py script (that launches the application)

    TODO:
        Remove this second argument (path)
    """
    str_ex = os.path.join(os.path.dirname(path), "examples",
                          "example%s.cfg" % number)
    subprocess.call(["python",
                     os.path.join(path, "run.py"), "-c", str_ex])

if __name__ == '__main__':
    if __package__ is None:
        os.sys.path.append(dir_app)

    # list of available examples
    idx_ex = [i for i in range(len(EX))]
    idx_ex = [str(i + 1) for i in range(len(EX))]

    if len(sys.argv) > 1:
        if 'all' in sys.argv:
            lst_run = idx_ex
        else:
            lst_run = sys.argv[1:] 
        for i in lst_run:
            run_example(i, dir_app)
    elif len(sys.argv) == 1:
        for i in idx_ex:
            print('*' * 80)
            print("EXEMPLE %s : %s" % (i, EX[int(i)-1]))
        MSG = 'Choisissez un exemple (%s-%s) :' % (idx_ex[0], idx_ex[-1])
        #PYTHON2/3 comptability
        try:
            reponse = raw_input(MSG).strip()
        except NameError:
            reponse = input(MSG).strip()
        again = True
        while again:
            again = False
            if reponse == 'all':
                for i in idx_ex:
                    run_example(i, dir_app)
            if reponse == 'q':
                sys.exit(0)
            elif reponse in idx_ex:
                run_example(reponse, dir_app)
            else:
                again = True
                #PYTHON2/3 comptability
                try:
                    reponse = raw_input(MSG).strip()
                except NameError:
                    reponse = input(MSG).strip()
    