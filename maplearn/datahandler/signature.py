# -*- coding: utf-8 -*-
"""
Chart dataset

This class makes charts about a dataset:

* spectral signature
* temporal signature

Example:
    >>> from maplearn.datahandler.loader import Loader
    >>> from maplearn.datahandler.signature import Signature
    >>> ldr = Loader('iris')
    >>> sig = Signature()
    >>> sig.plot(ldr.X, title='test')
"""
from __future__ import unicode_literals
import logging

import numpy as np
import matplotlib.pyplot as plt

#from matplotlib.ticker import MaxNLocator
#from matplotlib.dates import date2num, DateFormatter
from matplotlib.dates import date2num
# Turn interactive plotting off
plt.ioff()
logger = logging.getLogger('maplearn.' + __name__)


class Signature(object):
    """
    Makes charts about a dataset:
    
    * one global graph
    * one graph per class in samples (if samples are available)

    Args:
        * dates (list): list of dates (if temporal dataset)
    """

    def __init__(self, dates=None):
        self.dates = dates

    def plot(self, data, title, label='', features=None, out_file=None):
        """
        Affichage graphique synthétique des profils
        Entrées :
            - data = matrice avec les données MODIS
            - aGr = vecteur traduisant les groupes
            - out_file : nom de fichier en sortie
            - labels : nom de la variable en Y
            - y_lim [min, max] : limites en Y du graphique

        """
        logger.debug('Préparation du graphique...')
        if not isinstance(data, np.ndarray):
            data = np.array(data)
        profil = {'min': np.nanmin(data, axis=0),
                  'max': np.nanmax(data, axis=0),
                  'med': np.median(data, axis=0)}

        fig = plt.figure(figsize=(7, 5))
        ax0 = fig.add_subplot(1, 1, 1)

        # limites en ordonnée comprises entre 0 et 100
        if self.dates is not None:
            ax0.set_xlim([self.dates[0], self.dates[-1]])
        if label != '':
            ax0.set_ylabel(label)
        if features is not None:
            ax0.set_xlabel(features)

        if self.dates is not None:
            xlab = date2num(self.dates)
            fig.autofmt_xdate()
        else:
            xlab = range(1, data.shape[1] + 1)

        # Ajout des lignes graphiques (plot de chaque ligne)
        line, = ax0.plot(xlab, profil['med'], 'o--', color='0.5')
        line, = ax0.plot(xlab, profil['min'], '--', color='0.75')
        line, = ax0.plot(xlab, profil['max'], '--', color='0.75')

        ax0.set_title(title)

        if out_file is not None:
            plt.savefig(out_file, format='png', bbox_inches='tight')
            logger.info(u'Graphique enregistré (%s)', out_file)

        plt.close(fig)
        fig = None

    def __color_box(self, boxplot, zorder, alpha):
        """
        Colours boxes in the boxplot

        Args:
            * boxplot: plot onto put some colours
            * zorder?
            * alpha: transparency setting
        """
        for box in boxplot['boxes']:
            # change outline color & fill color
            box.set(color='darkgrey', linewidth=2, facecolor='lightgrey',
                    zorder=zorder + 1, alpha=alpha)

        # change color and linewidth of the whiskers
        for whisker in boxplot['whiskers']:
            whisker.set(color='darkgrey', linewidth=2, linestyle='-',
                        zorder=zorder + 2, alpha=alpha)

        # change color and linewidth of the caps
        for cap in boxplot['caps']:
            cap.set(color='darkgrey', linewidth=2, linestyle='-',
                    zorder=zorder + 3, alpha=alpha)

        # change color and linewidth of the medians
        for median in boxplot['medians']:
            median.set(color='darkgrey', linewidth=3, linestyle='-',
                       zorder=zorder + 4, alpha=alpha)

        # fliers <=> outliers
        plt.setp(boxplot['fliers'], color='white', marker='o', markersize=5.0,
                 zorder=zorder + 5, alpha=alpha)

    def boxplot(self, data, title='Signature', features=None, out_file=None):
        """
        Plots signature using a boxplot chart

        Args:
            * data: dataset to chart
            * title (str): title of the chart
            * features (list): features names
            * out_file (str): path to the output file
        """
        logger.debug('Préparation du graphique...')
        fig, ax1 = plt.subplots(nrows=1, ncols=1)  # , figsize=(7, 5)

        # Add a horizontal grid to the plot, but make it very light in color
        # so we can use it for reading data values but not be distracting
        ax1.yaxis.grid(True, linestyle='-', which='major', color='grey',
                       alpha=0.5, zorder=0)

        bp = ax1.boxplot(data, notch=1, patch_artist=True, vert=1,
                         whis=1.3)
        self.__color_box(bp, zorder=0, alpha=1)

        ax1.set_title(title)
        if features is not None:
            # TODO : ajouter des vérifications
            ax1.set_xticklabels(features)

        if out_file is not None:
            plt.savefig(out_file, bbox_inches='tight')
            logger.info(u'Graphique enregistré (%s)', out_file)

        plt.close(fig)
        fig = None

    def boxplot_classe(self, data, data_classe, title='', features=None,
                       out_file=None):
        """
        Creates a boxplot for the whole dataset and adds boxplot corresponding
        to one class to show signatures

        Args:
            * data: data to chart
            * data_class: data of one class
            * title (str): title of the boxplot
            * features (list): list of features to plot
            * out_file (str): path to the file to save the chart in
        """
        logger.debug('Initializing chart...')

        positions = [i for i in range(1, data.shape[1] + 1)]
        fig, ax1 = plt.subplots(figsize=(7, 5))

        # Add a horizontal grid to the plot, but make it very light in color
        # so we can use it for reading data values but not be distracting
        ax1.yaxis.grid(True, linestyle='-', which='major', color='grey',
                       alpha=0.5, zorder=0)

        bp = plt.boxplot(data, notch=1, patch_artist=True, vert=1,
                         whis=1.3, positions=[i - .1 for i in positions])

        self.__color_box(bp, zorder=0, alpha=0.5)

        plt.hold(True)  # superposition d'un second graphe
        bp = plt.boxplot(data_classe, notch=1, patch_artist=True, vert=1,
                         whis=1.3, positions=[i for i in positions])

        self.__color_box(bp, zorder=6, alpha=1)

        ax1.set_title(title)
        if features is not None:
            # TODO : ajouter des vérifications
            ax1.set_xticklabels(features)
        else:
            ax1.set_xticklabels(positions)

        if out_file is not None:
            try:
                plt.savefig(out_file, format='png', bbox_inches='tight')
            except IOError as e:
                logger.error("Missing folder : %s", out_file)
                raise IOError(e.message)
            else:
                logger.info('Chart saved in %s', out_file)

        # plt.show()
        plt.close(fig)
        fig = None
