# -*- coding: utf-8 -*-
"""
Samples

This class handles samples and their labels:
* counts how many samples for each class


Scripts sur la manipulation des echantillons
Created on Thu Jan 24 11:46:15 2013

@author: thomas_a
"""
from __future__ import print_function
from __future__ import unicode_literals

from collections import Counter
import logging

import numpy as np

logger = logging.getLogger('maplearn.' + __name__)


class Echantillon(object):
    """
    Samples labels used in PackData class

    Args:
        * Y (array): vector with samples' labels
        * codes (dict): dictionnary with labels code and associated description

    Attributes:
        * summary ()
        * dct_codes (dict): dictionnary with labels code and associated
                            description

    Property:
        * Y (array): vector containing labels of samples (codes)
    """

    def __init__(self, Y, codes=None):
        self.__y = None
        self.summary = None
        self.dct_codes = codes
        self.Y = Y

    def count(self):
        """
        Summarizes labels of each class (how many samples for each class)
        """
        ukeys = np.unique(self.__y)
        bins = ukeys.searchsorted(self.__y)
        l_nb = np.bincount(bins)
        self.summary = dict(zip(ukeys, l_nb))

    def convert(self):
        """
        Conversion between codes
        """
        if self.dct_codes is None:
            logger.error('Unable to convert using new codes')
        else:
            self.__y = np.array([self.dct_codes[i] for i in self.__y])

    def libelle2code(self):
        """
        Converts labels' names into corresponding codes
        """
        if self.__y.dtype == 'S1':
            self.convert()
        else:
            logger.info("Y already contains numerical values")

    @property
    def Y(self):
        """
        Samples (as a vector)
        """
        return self.__y

    @Y.setter
    def Y(self, Y):
        """
        Sets samples after checking matching labels <-> names
        """
        if Y.ndim != 1:
            str_msg = "Y should be one dimension (got %i)" % Y.ndim
            logger.critical(str_msg)
            raise IndexError(str_msg)

        # gestion de la nomenclature en fonction des codes donnés en entrée
        # et des valeurs observées dans Y
        if self.dct_codes is not None:
            codes_y = list(np.unique(Y))
            tmp = [c for c in codes_y if c not in self.dct_codes.keys()]
            # creation de nouveaux codes si manquent dans nomenclature
            if len(tmp) > 0:
                logger.info('Following labels will be created:')
                for code in tmp:
                    self.dct_codes[code] = str(code)
                    logger.info('New label : %i (%s)', code,
                                self.dct_codes[code])

            if len(self.dct_codes) > len(codes_y):
                logger.warning('Label(s) without sample')
        else:
            self.dct_codes = dict(zip(np.unique(Y),
                                      [str(i) for i in np.unique(Y)]))

        # Si plusieurs classes ont le même code => agréger ces classes
        counts = Counter(self.dct_codes.values())
        duplic = [k for k in counts if counts[k] > 1]
        if len(duplic) > 0:
            logger.debug('%i label(s) to recode', len(duplic))
            for libelle in duplic:
                codes = [k for k in self.dct_codes
                         if self.dct_codes[k] == libelle]
                for code in codes[1:]:
                    Y[Y == code] = codes[0]
                    self.dct_codes.pop(code)

                print('\t* [%s] => %i (%s)' % (','.join([str(code) for code
                      in codes[1:]]), codes[0], self.dct_codes[codes[0]]))
            logger.info('%i code(s) recoded(s)', len(duplic))

        # Affectation de Y et compte des echantillons par classe
        self.__y = Y
        self.count()

    def __str__(self):

        ukeys = np.unique(self.__y)
        if len(ukeys) < 1:
            return "\n### Echantillons ### \n* Aucun echantillon disponible"

        str_msg = "\n### Description des echantillons ###"
        str_msg += "\n%i Echantillons - %i classes : \n  " % (len(self.__y),
                                                              len(ukeys))
        n_congalton = 0
        for i in sorted(self.summary.keys()):
            str_msg += "\n* %s (%i) : %i echantillons " % (self.dct_codes[i],
                                                           i, self.summary[i])
            if self.summary[i] < 50:
                n_congalton += 1
                str_msg += "(1)  "
        str_msg += "  \n  "
        # Affichage des classes avec insuffisamment d'échantillons
        if n_congalton > 0:
            str_msg += "\n **WARNING :**\n"
            str_msg += "*(1) %i classe(s) avec trop peu d'echantillons " \
                       % n_congalton
            str_msg += "[regle empirique de Congalton : >= 50 echantillons]*\n"

        return str_msg
