# -*- coding: utf-8 -*-
"""
PackData: container for datasets

A PackData contains:

* samples (Y and X) to fit algorithms
    * Y is a vector with samples' labels
    * X is a matrix with samples' features
* data onto which algorithms make prediction

PackData checks if samples are compatible with data (same features...) and can
be used later with Machine Learning algorithms

Example:
    >>> import numpy as np
    >>> data = np.random.random((10, 5))
    >>> x = np.random.random((10, 5))
    >>> y = np.random.randint(1, 10, size=10) 
    >>> ds = PackData(x, y, data)
    >>> print(ds)
"""
from __future__ import print_function
from __future__ import unicode_literals

import os
from random import sample
import logging

import numpy as np
import pandas as pd
from sklearn.metrics import silhouette_samples
from sklearn import preprocessing

from maplearn.datahandler.echantillons import Echantillon
from maplearn.datahandler.signature import Signature
from maplearn.ml.reduction import Reduction
from maplearn.ml.distance import Distance
from maplearn.app.reporting import str_extend, str_table


logger = logging.getLogger('maplearn.' + __name__)

LIM_NFEAT = 15  # number of features that can be displayed at once


class PackData(object):
    """
    Dataset container with:

    * Y: a vector with labels of samples
    * X: matrix with features of samples
    * data: matrix with features to use for prediction

    Args:
        * X (array): 2d matrix with features of samples
        * Y (array): vector with labels of samples
        * data (array): 2d matrix with features
        * **kwargs: other parameters about dataset (features, na...)

    Attributes:
        * not_nas: vector with non-NA indexes
    """

    def __init__(self, X=None, Y=None, data=None, **kwargs):
        # échantillons :
        # Y=vecteur d'échantillons, X=matrice de données correspondantes
        self.__data = {'X': None, 'Y': None, 'data': None}
        self.not_nas = None  # vecteur listant les donnees non NAs
        self.__metadata = {'codes': None, 'features': None,
                           'source': None, 'na': np.nan,
                           'outfile': '', 'images': ''}
        self.__e = None
        for var in self.__metadata.keys():
            if var in kwargs:
                self.__metadata[var] = kwargs[var]

        self.load(X=X, Y=Y, data=data)

    @property
    def X(self):
        """
        X (array): 2d matrix with features of samples
        """
        return self.__data['X']

    @X.setter
    def X(self, x):
        """
        Sets features of samples
        """
        if x is None:
            self.__data['X'] = None
            logger.debug('X (re)initialized')
            return self.__data['X']
        if x.ndim != 2:
            logger.critical('Samples features should be a 2d matrix')

        # X is stored
        self.__data['X'] = np.copy(x)

        if self.__metadata['features'] is not None:
            if self.__data['X'].shape[1] != len(self.__metadata['features']):
                str_msg = 'Features (%i) in conflict with X (%i)' \
                         % (len(self.__metadata['features']),
                            self.__data['X'].shape[1])
                logger.critical(str_msg)
                raise IndexError(str_msg)
            else:
                self.features = self.__data['X']

    @property
    def Y(self):
        """
        Y (array): vector with labels of samples
        """
        return self.__data['Y']

    @Y.setter
    def Y(self, y):
        """
        Sets vector of samples' labels
        """
        if y is None:
            self.__data['y'] = None
            logger.debug('Y (re)initialized')
            return self.__data['Y']

        if isinstance(y, list):
            y = np.array(y)

        # vérifie qu'il y a suffisamment de classes d'échantillons
        if len(np.unique(y)) <= 1:
            raise ValueError('Number of classes too low : %i',
                             len(np.unique(y)))
        if y.ndim != 1:
            str_msg = 'Samples should be a vector. Got %id data.' % y.ndim
            raise ValueError(str_msg)
        self.__e = Echantillon(y, self.__metadata['codes'])

        # Y is stored
        self.__data['Y'] = np.copy(y)

    @property
    def data(self):
        """
        data (array): 2d matrix with features
        """
        return self.__data['data']

    @data.setter
    def data(self, data):
        """
        Sets the features of data to predict

        Args:
            * data (array): data to set
        """
        if data is None:
            logger.debug('Data (re)initialized')
            self.__data['data'] = None
            return self.__data['data']

        if data.ndim not in [2, 3]:
            raise IndexError('Data should be a 2d/3d matrix. %i dimension is\
                             not accepted' % data.ndim)
        self.__data['data'] = np.copy(data)

        # gestion des na
        self.not_nas = np.all(self.__data['data'] != self.__metadata['na'],
                              axis=1)
        n_na = len(self.not_nas[self.not_nas == False])
        if n_na >= self.__data['data'].shape[0]:
            s_msg = 'All data are considered NA [%s]' % self.__metadata['na']
            logger.critical(s_msg)
            raise ValueError(s_msg)
        elif n_na > 0:
            logger.warning('%i/%i (%.1f%%) NA values detected', n_na,
                           self.__data['data'].shape[0],
                           (n_na * 100.) / self.__data['data'].shape[0])

        # data is stored
        self.__data['data'] = np.copy(self.__data['data'][self.not_nas, :])

        # features
        if self.__metadata['features'] is not None:
            if self.__data['data'].shape[1] != len(self.__metadata['features']):
                str_msg = 'Features (%i) in conflict with dataset (%i)' \
                           % (len(self.__metadata['features']),
                              self.__data['data'].shape[1])
                logger.critical(str_msg)
                raise IndexError(str_msg)
        else:
            self.features = self.__data['data']

    @property
    def classes(self):
        """
        dict: labels classes and associated number of individuals
        """
        if self.Y is None:
            return None
        if self.__e.summary is None:
            self.__e.count()
        return self.__e.summary

    @property
    def features(self):
        """
        list: list of features of the dataset
        """
        return self.__metadata['features']

    @features.setter
    def features(self, items):
        """
        Sets the list of features

        Args:
            * items (list or array): list of features to set
        """
        if isinstance(items, list):
            self.__metadata['features'] = [str(i) for i in items]
        elif isinstance(items, np.ndarray):
            self.__metadata['features'] = [str(i) for i in
                                           range(items.shape[1])]
        elif isinstance(items, pd.DataFrame):
            self.__metadata['features'] = [str(i) for i in items.columns]
        else:
            raise TypeError('Data should be an array or a dataframe')
        logger.info('Dataset with %i features',
                    len(self.__metadata['features']))

    def load(self, X=None, Y=None, data=None, features=None):
        """
        Loads data to the packdata

        Args:
            * X (array): 2d matrix with features of samples
            * Y (array): vector with labels of samples
            * data (array): 2d matrix with features
            * features (list): list of features

        """
        logger.debug('Loading data in PackData...')
        if X is not None and Y is not None:
            # Check compatibility between X and Y (dimensions)
            if X.shape[0] != Y.shape[0]:
                str_msg = "Number of samples (%i) in conflict with \
                          number of datas (%i)" % (Y.shape[0], X.shape[0])
                logger.critical(str_msg)
                raise IndexError(str_msg)

            # Check if labels are non-values (<= 0)
            if np.any(Y <= 0):
                logger.warning('Labels <= 0 are excluded')
                X = X[Y > 0, :]
                Y = Y[Y > 0]

            self.X = X
            self.Y = Y

        if data is not None:
            self.data = data

        if features is not None:
            self.features = features

        logger.info('Datas loaded')
        self.__check()

    def __check(self):
        """
        Check compatibility between main properties of PackData:
        * X
        * Y
        * data
        """
        logger.debug('Checking dataset...')
        if self.X is None and self.Y is None and self.data is None:
            raise ValueError("Dataset is empty")

        # check compatibility based on number of features:
        if self.X is not None or self.Y is not None:
            if self.X.shape[0] != self.Y.shape[0]:
                raise IndexError("Numbers of features in data (%i) \
                                 and samples (%i) are different",
                                 self.X.shape[0], self.Y.shape[0])
        if self.data is not None and self.X is not None:
            if self.data.shape[1] != self.X.shape[1]:
                raise IndexError('Numbers of features in data (%i) and \
                                 samples (%i) are different',
                                 self.data.shape[1], self.X.shape[1])

        logger.info('Dataset checked => OK')

    def scale(self):
        """
        Normalizes data and X matrices
        """
        logger.debug('Normalizing dataset...')
        for i in ('data', 'X'):
            if self.__data[i] is not None:
                self.__data[i] = preprocessing.scale(self.__data[i])
        logger.debug('Dataset normalized')

    def reduit(self, meth='lda', ncomp=None):
        """
        Reduces number of dimensions of data and X

        Args:
            * meth (str): reduction method to apply
            * ncomp (int): number of dimensions expected
        """
        logger.debug("Reducing dataset's dimensions...")

        red = Reduction(data=self, algorithms=meth,
                        features=self.features, ncomp=ncomp)
        if self.data is not None:
            (self.__data['data'], self.__data['X'], self.__metadata['features']) = red.run()
        else:
            (self.__data['X'], _, self.__metadata['features']) = red.run()
        logger.info('Dataset reduced to %i dimensions', 
                    len(self.__metadata['features']))

    def separabilite(self, metric='euclidean'):
        """
        Performs separability analysis between samples

        Arg:
            * metric (str): name of the distance used
        """
        logger.debug('Performing separability analysis (%s)...', metric)
        dist = Distance()
        mat_dist = dist.run(x=self.__data['X'], meth=metric)
        sep = silhouette_samples(X=mat_dist, labels=self.__data['Y'],
                                 metric="precomputed")
        print('\n## Analyse de separabilite (%s)\n' % metric)
        header = ['classe', 'mean', 'std', 'min', 'max', 'separable']
        dct_sep = {i:[] for i in header}
        for i in np.unique(self.__data['Y']):
            sep_cl = sep[np.where(self.__data['Y'] == i)]
            i = str(int(i))
            # Recode les valeurs pour affichage
            m_cl = np.mean(sep_cl)
            if m_cl > .5:
                str_msg = "++"
            elif m_cl > .25 and m_cl <= .5:
                str_msg = "+"
            elif m_cl < -.25 and m_cl >= -.5:
                str_msg = "-"
            elif m_cl < -.5:
                str_msg = "--"
            else:
                str_msg = ""
            dct_sep['classe'].append(i)
            dct_sep['mean'].append('%.2f' % m_cl)
            dct_sep['std'].append('%.2f' % np.std(sep_cl))
            dct_sep['min'].append('%.2f' % np.min(sep_cl))
            dct_sep['max'].append('%.2f' % np.max(sep_cl))
            dct_sep['separable'].append(str_msg)
        
        print(str_table(header=header, size=30, **dct_sep))
        logger.info('Separability analysis (%s) done', metric)

    def balance(self, seuil=None):
        """
        Balance samples and remove some individuals within the biggest classes.

        Args:
            * seuil (int): max number of samples inside a class
        """
        # convertir en pandas df => + facile pour faire la suite
        logger.debug('Balancing samples...')
        a_count = np.array(list(self.__e.summary.values()))
        if seuil is not None:
            seuil = int(seuil)
        else:
            # TODO : trouver ref avec regle empirique pour definir ce seuil
            if len(self.__e.summary) > 3:
                # seuil défini par défaut = triple de la fréquence médiane
                seuil = int(np.median(a_count) * 3)
            else:
                # seuil quand peu de classes = nbres d'échantillons dans la
                # classe avec le moins d'échantillons
                seuil = int(np.min(a_count))
        if np.max(a_count) <= seuil:
            print("Reequilibrage des echantillons (seuil : %i) => inutile"
                  % seuil)
        else:
            print("Reequilibrage des echantillons (seuil : %i)" % seuil)
            a_idx = np.ones(self.__data['Y'].shape, dtype=np.byte)
            # Liste des classes dominantes (> seuil)
            lst_cl_dom = [k for k, v in self.__e.summary.items() if v > seuil]
            for l_os in lst_cl_dom:
                a_idx_os = np.where(self.__data['Y'] == l_os)
                print("Classe %i : %i => %i echantillons a eliminer"
                      % (l_os, len(a_idx_os[0]), len(a_idx_os[0]) - seuil))
                # selection aleatoire des echantillons a eliminer (code 0)
                a_idx_sel = sample(range(len(a_idx_os[0])),
                                   len(a_idx_os[0]) - seuil)
                a_idx[a_idx_os[0][a_idx_sel]] = 0
            # application de la selection
            self.__data['Y'] = np.copy(self.__data['Y']
                                       [np.where(a_idx == 1)[0]])
            self.__data['X'] = np.copy(self.__data['X']
                                       [np.where(a_idx == 1)[0], :])
            # description des echantillons apres equilibrage
            self.__e.Y = self.__data['Y']
        logger.info('Samples balanced')

    def plot(self, out_file):
        """
        Charts the dataset (signature):
        * one chart for the whole samples
        * one chart per samples' class

        Args:
            * out_file (str): path to files to saved charts in
        """
        self.__metadata['outfile'] = out_file
        logger.debug('Preparing charts of dataset')
        sig = Signature()
        str_title = 'Signature spectrale du jeu de donnees\n'
        if self.data is None and self.X is not None:
            data = self.X
        elif self.X is not None:
            data = self.data
        else:
            logger.error('No available data')
            return

        if data.shape[1] > LIM_NFEAT:
            sig.plot(data=data, title=str_title,
                     out_file=self.__metadata['outfile'], features=None)
        else:
            sig.boxplot(data=data, title=str_title,
                        out_file=self.__metadata['outfile'],
                        features=self.__metadata['features'])

        if self.X is not None and self.Y is not None:
            self.__metadata['images'] = '\n\n'
            for i in np.unique(self.Y):
                str_title = 'Signature spectrale : classe %i\n' % i
                str_file = self.__metadata['outfile'].replace('.', 'cl%i.' % i)
                if data.shape[1] <= LIM_NFEAT:
                    sig.boxplot_classe(data=self.X,
                                       data_classe=self.X[self.Y == i, :],
                                       title=str_title,
                                       out_file=str_file,
                                       features=self.__metadata['features'])
                    self.__metadata['images'] += '![%s](%s){: .sigclass} ' \
                        % (str_title, os.path.basename(str_file))
            self.__metadata['images'] += '\n\n'
        logger.info('Charts created')

    def __str__(self):

        str_msg = "\n### Description du jeu de donnees ###\n"
        if self.__data['data'] is None and self.__data['X'] is None:
            str_msg += '\t=> Aucune donnee disponible'
        else:
            if self.__data['data'] is None:
                _data = pd.DataFrame(self.__data['X'])
            else:
                _data = pd.DataFrame(self.__data['data'])

            n_data = _data.shape[0]
            n_feat = _data.shape[1]
            pd.set_option('display.precision', 2)
            stats = str(_data.describe())

            str_msg += '\n\n![%s](%s){: .sig}\n\n' \
                       % ("sign", os.path.basename(self.__metadata['outfile']))
            str_msg += "* %i donnees\n" % n_data
            str_msg += "* %i feature(s) : %s\n" % (n_feat, list(_data))
            str_msg += "* Resume statistique :\n\n" 
            str_msg += '<pre>'+stats+'</pre>\n'
        str_msg += str(self.__e) + '\n'
        str_msg += self.__metadata['images'] + '\n'
        #str_msg += stats + '\n'
        return str_msg
