# -*- coding: utf-8 -*-
"""
Writes data in a file


This class is tu be used with PackData. It puts data into one file (different
formats are useable).


"""
from __future__ import print_function
import os
import logging

from maplearn.filehandler.shapefile import Shapefile
from maplearn.filehandler.excel import Excel
from maplearn.filehandler.imagegeo import ImageGeo

logger = logging.getLogger('maplearn.' + __name__)


class Writer(object):
    """
    Writes data in a file (different formats available)
    
    Args:
        * source (str): path towards the file to write data into
        * **kwargs:
            * origin (str): path to the original file used as a model 
    """
    def __init__(self, source, **kwargs):
        self.src = str(source).strip()
        self.__driver = None
        self.__to_file()
        self.__origin = None
        if 'origin' in kwargs:
            self.__origin = kwargs['origin']

    def __to_file(self):
        """
        Accesses the file specified in the constructor, before writing into it
        """
        logger.info('Access to the file : %s', self.src)
        s_ext = os.path.splitext(self.src)[1].lower()
        
        if s_ext in ['.xls', '.xlsx']:
            self.__driver = Excel(self.src)
        elif s_ext == '.shp':
            self.__driver = Shapefile(self.src)
        elif s_ext in ['.tif', '']:
            self.__driver = ImageGeo(self.src)
        else:
            raise IOError("Unknown format file: %s" % self.src)
        if self.__driver is not None:
            self.__driver.open_()

    def run(self, data, fichier):
        """
        Writes data into a file
        
        Args:
            * data (pandas dataframe): dataset to write
            * fichier (str): path towards the file to write data into
        """
        logger.info('Writing data into file %s...', fichier)
        self.__driver.write(fichier, data, origin=self.__origin)
