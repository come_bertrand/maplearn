# -*- coding: utf-8 -*-
"""
Mapping Learning execution script


This script is the main entry to play with mapping learning. Just specify a
well-formatted configuration file and run.

The configuration file is described on
https://bitbucket.org/thomas_a/maplearn/wiki/configuration. A few examples of
configuration are also available in "examples" sub-folder.

Example::

    $ python run.py -c /path/to/configfile

TODO:
    Add more arguments to override configuration based on files. Arguments that
    should be updatable this way : k-fold, algorithms...

"""
from __future__ import print_function

import os
import sys
import getopt
#from datetime import datetime

if __name__ == '__main__' and __package__ is None:
    os.sys.path.append(
        os.path.dirname(os.path.abspath(__file__)))

from maplearn.app.main import Main
from maplearn.app.config import Config
from maplearn.app.reporting import ReportWriter

def run(argv):
    """
    Run the application with arguments given in argv.

    Args:
        argv (dict): parameters to configure application
            * configfile (str) : path to configuration file

    Examples:

        >>> main('-c /path/to/config/file')
    """
    configfile = 'config.cfg'
    try:
        opts, _ = getopt.getopt(argv, "hc:", ["configfile=", ])
    except getopt.GetoptError:
        print('run.py -c <configfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('run.py -c <configfile>')
            sys.exit()
        elif opt in ("-c", "--configfile"):
            configfile = arg
    print('Config file : %s', configfile)

    # chargement de la configuration
    cfg = Config(configfile)

    # redirection des print vers 1 fichier
    report_file = os.path.join(cfg.dirs['sortie'], 'maplearn_report')
    report_writer = ReportWriter(report_file)
    sys.stdout = report_writer

    print(cfg)

    ml = Main(cfg.dirs['sortie'],
                    type=cfg.process['type'],
                    algorithm=cfg.process['algorithm'],
                    metric=cfg.process['distance'],
                    kfold=cfg.process['kfold'],
                    codes=cfg.entree['codes'])

    # TODO: PATCH tout moche à retirer dés que possible
    if cfg.process['type'] == 'clustering' and \
            cfg.entree['echantillons'] is None:
        cfg.entree['echantillons'] = [cfg.entree['data'], ]

    for _file in cfg.entree['echantillons']:
        __params = {i: cfg.entree[i] for i in ('classe', 'classe_id',
                                               'features', 'na')}
        ml.load(_file, **__params)
        if not cfg.entree['data'] is None:
            ml.load_data(cfg.entree['data'], features=cfg.entree['features'])
        basename = os.path.splitext(os.path.basename(_file))[0]
        ml.dataset.plot(os.path.join(cfg.dirs['sortie'],
                                      'sig_%s.png' % basename))
        print(ml.dataset)
        ml.preprocess(**cfg.preprocess)

        if cfg.preprocess['scale'] or cfg.preprocess['reduction'] or \
                cfg.preprocess['balance']:
            ml.dataset.plot(os.path.join(cfg.dirs['sortie'],
                                          'sig2_%s.png' % basename))
            print(ml.dataset)
        ml.process(optimisation=cfg.process['optimisation'],
                prediction=cfg.process['prediction'])

    report_writer.close()

if __name__ == "__main__":
    run(sys.argv[1:])
