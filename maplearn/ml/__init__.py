# -*- coding: utf-8 -*-
"""
Machine Learning modules

These modules contain the Machine Learning part of Mapping Learning :

* Machine: abstract class of a machine learning processor, one or more 
           algorithms can be applied
* Classification: processor of classification
* Clustering: I let you guess
* Regression: do you really need an explanation ?
* Distance: computes distance using different formulas
* Reduction: reduces dimensions of a dataset
* Confusion: class about confusion matrix

"""