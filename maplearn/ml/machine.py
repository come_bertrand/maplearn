# -*- coding: utf-8 -*-
"""
Machine Learning class

Fits and predict result using one or several machine learning algorithms.

*This is an abstract class that should not be used directly*. Use instead one
one of the these classes:

* Classification
* Clustering
* Regression

"""
from __future__ import print_function
import os
import logging
import numpy as np

logger = logging.getLogger('maplearn.' + __name__)


class Machine(object):
    """
    Class to apply one or several machine learning algorithms on a given
    dataset.
    
    
    """
    ALL_ALGOS = dict()
    # Parametres lies a performance
    # Cache size = taille en RAM dispo (en Mo)
    # N_JOBS : nombre de processeurs à utiliser (-1 = tous)
    _params = {'cache_size': 24000000, 'n_jobs': -1}

    def __init__(self, data, algorithms=None, **kwargs):
        self._algorithms = dict()  # algorithmes a appliquer
        self.clf = None  # algorithme en cours d'utilisation
        self._data = None  # jeux de données sur lequel travailler
        self._fitted = False
        self._na = np.nan
        self.result = None  # resultat des predictions des classifs

        # dossier en sortie
        if 'dirOut' not in kwargs or kwargs['dirOut'] is None:
            self.dir_out = os.getcwd()
            logger.warning('Output folder not mentionned => %s will be used',
                           self.dir_out)
        else:
            self.dir_out = kwargs['dirOut']
        if not os.path.exists(self.dir_out):
            os.makedirs(self.dir_out)
            logger.info('Folder %s missing => created', self.dir_out)

        # chargement des données
        if data is not None:
            self.load(data)
        if 'na' in kwargs:
            self._na = kwargs['na']

    @property
    def algorithms(self):
        """
        Renvoie la liste des algorithmes qui seront utilises
        """
        return [i for i in self._algorithms.keys()]

    @algorithms.setter
    def algorithms(self, algorithms):
        """
        Sélectionne les algorithmes à utiliser parmi tous les algorithmes
        existants
        """

        if Machine.ALL_ALGOS is None or len(Machine.ALL_ALGOS) < 1:
            raise KeyError('No algorithm available')
        if isinstance(algorithms, (str, type(u""))):
            algorithms = [algorithms, ]
        if algorithms is None or len(algorithms) == 0:
            self._algorithms = Machine.ALL_ALGOS
            self.__set_params()
            return None
        try:
            self._algorithms = {k: Machine.ALL_ALGOS[k] for k in algorithms}
        except KeyError:
            str_msg = 'Unknown algorithm(s) in :'
            str_msg += ','.join(algorithms)
            logger.critical(str_msg)
            raise KeyError(str_msg)
        logger.info('%i algorithms set', len(self._algorithms))
        self.__set_params()

    def __set_params(self):
        """
        Apply parameters to an algorithm
        """
        for i in self.algorithms:
            params = {k: Machine._params[k] for k in Machine._params.keys()
                      if k in self._algorithms[i].get_params()}
            if len(params) > 0:
                logger.debug('"%i" parameters to change in %s', len(params), i)
                self._algorithms[i].set_params(**params)
        logger.debug('Performance parameters applied')

    def predict_1(self, algo, export=False):
        """
        Predict a result using a given algotihm
        """
        self.__apply_1(algo)
        if export and self.dir_out is None:
            logger.error('EXPORT : missing output folder')
            export = False

        if not self._fitted:
            logger.warning("Algorithm %s not trained yet", algo)
            self.fit_1(algo)
            if not self._fitted:
                logger.error("Algorithme %s can't be trained", algo)
                return None
        logger.info('Predicting using %s', algo)

    def fit_1(self, algo):
        """
        Fits an algorithm to dataset
        """
        self._fitted = False
        self.__apply_1(algo)
        logger.info("Fitting algorithm %s", algo)

    def __apply_1(self, algo):
        """
        Use an algorithm
        """
        if algo not in self._algorithms.keys():
            raise Exception('Unknown algorithm : %s' % algo)
        self.clf = self._algorithms[algo]

    def load(self, data):
        """ Charge les données nécessaires à la classification d'un site et
        d'une année
        """
        logger.debug('Loading data for processing...')
        """
        # TODO comment tester qu'il s'agit bien d'un packdata ?
        if not isinstance(data, PackData):
            logger.critical('Waiting a Packdata')
        """
        if hasattr(data, 'X') and hasattr(data, 'Y') and hasattr(data, 'data'):
            self._data = data
        else:
            raise TypeError('Assuming a packdata to process')
        logger.info('Data loaded for processing')

    def run(self, predict=False):
        """
        Application de la classificaion suivant toutes les les méthodes
        demandées
        """
        print(self)
        for name in self._algorithms.keys():
            self.fit_1(name)
            if self._fitted and predict:
                self.predict_1(name)
        print('\n##Comparaison de(s) %i algorithme(s)##'
              % len(self._algorithms))

    def __str__(self):
        msg = '\n### Liste de(s) algorithme(s) ###'
        sklink = 'http://www.scikit-learn.org/stable/'
        msg += "\n\n*Plus d'informations sur les algorithmes et les parametres \
                sont disponibles sur le site de [scikit-learn](%s)*\n\n" % sklink
        for k in self._algorithms:
            msg += '\n* **%s** : *%s*' % (k, self._algorithms[k])
        msg += '\n'
        return msg
