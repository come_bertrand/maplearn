import logging
import json
from collections import defaultdict

from maplearn.app.config import splitter
from maplearn.kb.filter_engine import FilterEngine
from maplearn.kb.check_factory import CheckFactory


class KnowledgeEngine(object):
    def __init__(self, cfg):
        self.process_attributes = cfg.process
        self.rules_classification = defaultdict(list)
        self.rules_clustering = defaultdict(list)
        self.rules_regression = defaultdict(list)
        self.warnings = []

        if cfg.process['type'] == 'classification':
            with open(cfg.knowledge['file_classification']) as f:
                rules_data = json.loads(f.read())
                self.__init_rules(self.rules_classification, rules_data)

        elif cfg.process['type'] == 'clustering':
            with open(cfg.knowledge['file_clustering']) as f:
                rules_data = json.loads(f.read())
                self.__init_rules(self.rules_clustering, rules_data)

        elif cfg.process['type'] == 'regression':
            with open(cfg.knowledge['file_regression']) as f:
                rules_data = json.loads(f.read())
                self.__init_rules(self.rules_regression, rules_data)

    def __init_rules(self, rule_container, rules_data):
        for rule in rules_data:
            check_function = (CheckFactory.get_check_function(
                rule['check']['function'],
                rule['check']['target'],
                rule['check']['attributes']))
            filter_engine = FilterEngine(rule['filter'],
                                         rule['knowledge_target'],
                                         rule['message'])
            rule_container[check_function].append(filter_engine)

    def apply_to_benchmark(self, benchmark):
        self.__apply_rules_to_benchmark(self.rules_classification, benchmark)
        self.__apply_rules_to_benchmark(self.rules_clustering, benchmark)
        self.__apply_rules_to_benchmark(self.rules_regression, benchmark)

        for warning in self.warnings:
            logging.warning(warning)

    def __apply_rules_to_benchmark(self, rules_container, benchmark):
        for check_function, list_filters in rules_container.items():
            for filter in list_filters:
                if self.__rule_apply(filter, benchmark):
                    if not check_function(benchmark):
                        self.warnings.append(filter.rule_message)

    def __rule_apply(self, filter, benchmark):
        target = self.__get_target_in_machine(filter.target, benchmark.machine)
        if target is not None:
            if filter.evaluate(target=target,
                               dataset=benchmark.dataset,
                               process=self.process_attributes):
                return True
        return False

    def __get_target_in_machine(self, filter_target, machine):
        if filter_target == 'all':
            return True
        targets = splitter(filter_target)
        algo_in_machine = set(machine.algorithms)
        for target in targets:
            if target in algo_in_machine:
                return machine._algorithms[target]
        return None
