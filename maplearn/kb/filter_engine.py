class FilterEngine(object):
    def __init__(self, filter_str, filter_target, rule_message):
        self.filter_str = filter_str
        self.target = filter_target
        self.rule_message = rule_message

    def evaluate(self, **kwargs):
        if not self.filter_str:
            return True
        return eval(self.filter_str)
