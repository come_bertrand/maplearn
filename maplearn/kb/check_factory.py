from collections import Counter
from operator import attrgetter, itemgetter


class CheckFactory(object):
    @classmethod
    def get_check_function(cls, function_name, target, attributes):
        if hasattr(cls, function_name):
            function = getattr(cls, function_name)
            return CheckFunction(function, target, attributes)
        else:
            raise AttributeError("No check function named : {}".
                                 format(function_name))

    @classmethod
    def min_num_echantillons_per_class(cls, labels, seuil=50):
        c = Counter(labels)
        nb_under_seuil = len([class_name for class_name in c
                              if c[class_name] < seuil])
        if nb_under_seuil:
            return False
        return True


class CheckFunction(object):
    def __init__(self, function, function_target, function_attributes):
        self.function = function
        self.target = function_target
        self.target_getter = attrgetter(function_target)
        self.attributes = function_attributes

    def __call__(self, benchmark):
        data = self.target_getter(benchmark)
        return self.function(data, **self.attributes)

    def __hash__(self):
        attr = tuple(sorted([(att_name, att_value) for att_name, att_value in
                             self.attributes.items()], key=itemgetter(0)))
        return hash((self.function.__name__, self.target, attr))
