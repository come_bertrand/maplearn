#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Mapping Learning Configuration


This class configuration Mapping Learning based on a configuration file. It is
also able to re-write a re-usable configuration file based on some attributes.

"""
from __future__ import print_function
from __future__ import unicode_literals

import os
import re
from datetime import datetime
from shutil import copyfile

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

import logging
from maplearn import dir_app

logger = logging.getLogger('maplearn.' + __name__)


def splitter(text):
    """
    Splits a character string based on several separators and remove useless
    empty characters.

    Args:
        text (str) : character string to split

    Returns:
        list: list of stripped character strings, None elsewhere
    """
    try:
        text = re.split("[,;|]", text)
    except TypeError:
        return None
    text = [i.strip() for i in text]
    return text


def cvert_bool(text):
    """
    Converts the parameter in boolean, accepting several codes

    Args:
        text (several types expected): variable to be converted in boolean

    Returns:
        bool: true if text can be converted, else false
    """
    if isinstance(text, bool):
        return text
    try:
        text = text.lower()
    except AttributeError:
        raise TypeError('Text expected. Got %s' % type(text))

    return text in ["t", "true", "1"]

def cvert_text(text):
    """
    Clean a given character string

    Args:
        text (str) : character string to be cleaned

    Returns:
        str : cleaned text, or None
    """
    try:
        text = text.strip()
    except AttributeError:
        pass
    if text == '':
        text = None
    return text


class Config(object):
    """
    This class is the medium between a configuration file and the applicaton.
    It is able to load and check a configuration file (Yaml) and rewrite a new
    configuration file (that can be re-used by Mappling Learning later).

    Args:
        config_file (str) : path to a configuration file

    The class attributes described below reflects the sections in configuration
    file.

    Attributes:
        entree (dict): path to samples and dataset files, list of features to
                       use...
        preprocess (dict) : which preprocessing step(s) to apply
        process (dict) : which processes to apply (list of algorihms...)
        dirs (dict): pathsto datasets, output and application files
    """
    __METADATA = dict(
            preprocess={'scale': 'Scale',
                          'balance': 'Reequilibrage des echantillons',
                          'separabilite': 'Analyse de separabilite des \
                          echantillons'})
    entree = dict()
    preprocess = {'scale': True, 'balance': False, 'reduction': None,
                  'ncomp': None, 'separabilite': False}
    process = {'type': None, 'kfold': None, 'optimisation':False,
               'prediction':False}
    dirs = dict()
    metadata = dict(title='MAPPING LEARNING (%s)'
                    % datetime.today().strftime('%d/%m/%Y'))

    def __init__(self, file_config):
        if not os.path.exists(file_config):
            str_msg = "Configuration file is missing (%s)" % file_config
            logger.critical(str_msg)
            raise configparser.Error(str_msg)
        logger.debug('Configuration file : %s', file_config)
        self._file_cfg = file_config
        self.read()

    def check(self):
        """
        Check that parameters stored in attributes are correct

        Returns:
            int : number of issues detected
        """
        nb_pbs = 0

        if not os.path.exists(self.dirs['sortie']):
            os.makedirs(self.dirs['sortie'])

        # Converts in boolean
        for param in ['scale', 'balance', 'separabilite']:
            self.preprocess[param] = cvert_bool(self.preprocess[param])
        for param in ['optimisation', 'prediction']:
            self.process[param] = cvert_bool(self.process[param])

        # liste des source(s) en entree
        for i in ['echantillons', 'features']:
            self.entree[i] = splitter(self.entree[i])
        if not self.entree['echantillons'] is None:
            for fichier in self.entree['echantillons']:
                if fichier in ['iris', 'digits']:
                    continue
                if not os.path.exists(fichier):
                    logger.critical("Fichier source (%s) absent", fichier)
                    nb_pbs += 1

        if not self.entree['data'] is None:
            if not os.path.exists(self.entree['data']):
                logger.critical("Fichier source (%s) absent",
                                self.entree['data'])
                nb_pbs += 1

        # Get list of algorithm(s)
        self.process['algorithm'] = splitter(self.process['algorithm'])

        # Converts empty character strings in None
        for param in ['classe', 'classe_id', 'na']:
            self.entree[param] = cvert_text(self.entree[param])
        for param in ['reduction', ]:
            self.preprocess[param] = cvert_text(self.preprocess[param])

        logger.info('Configuration checked : %i issue(s)', nb_pbs)
        
        if nb_pbs == 0:
            # copy style file necessary to report
            copyfile(os.path.join(dir_app, 'maplearn', 'app', 'style',
                                  'style.css'),
                     os.path.join(self.dirs['sortie'], 'style.css'))
        return nb_pbs

    def read(self):
        """
        Load parameters from configuration file and put them in corresponding
        class attributes

        Returns:
            int : number of issues got when reading the file
        """
        cfgparser = configparser.ConfigParser()
        logger.debug('Trying to read configuration...')
        cfgparser.read(self._file_cfg)

        # Définition des données en entrée
        lst = ['echantillons', 'classe_id', 'classe', 'features', 'data', 'na']
        for param in lst:
            try:
                self.entree[param] = cfgparser.get('entree', param).strip()
            except configparser.NoOptionError:
                self.entree[param] = None
            else:
                if self.entree[param] == '':
                    self.entree[param] = None

        # Pretraitements
        lst = ['scale', 'reduction', 'separabilite', 'balance']
        for param in lst:
            try:
                self.preprocess[param] = cfgparser.get('pretraitement',
                                                       param).strip()
            except configparser.NoOptionError:
                self.preprocess[param] = None

        try:
            self.preprocess['ncomp'] = cfgparser.getint('pretraitement',
                                                        'ncomp')
        except ValueError:
            pass

        lst = ['type', 'distance', 'algorithm', 'optimisation', 'prediction']
        for param in lst:
            self.process[param] = cfgparser.get('traitement', param).strip()
        try:
            self.process['kfold'] = cfgparser.getint('traitement', 'kfold')
        except ValueError:
            logger.warning('k-fold non spécifié ou incorrect (%s) => 3-fold',
                           self.process['kfold'])
            self.process['kfold'] = 3

        # distance used for computations
        if self.process['distance'] is None or self.process['distance'] == '':
            self.process['distance'] = 'euclidean'

        if cfgparser.get('sortie', 'dossier').strip() == '':
            self.dirs['sortie'] = os.path.join(os.getcwd(), 'tmp')
        else:
            self.dirs['sortie'] = os.path.normpath(cfgparser.get('sortie',
                                                                 'dossier'))
        # get legend of classes if given in configuration file
        self.entree['codes'] = None
        if cfgparser.has_section('codes'):
            self.entree['codes'] = dict(cfgparser.items('codes'))
            for i in self.entree['codes']:
                self.entree['codes'][int(i)] = self.entree['codes'].pop(i)
            logger.info('Legend read with %i classes',
                        len(self.entree['codes']))
        cfgparser = None
        nb_pbs = self.check()
        if nb_pbs == 0:
            logger.info('Configuration file read !')
        else:
            logger.critical('%i issue(s) when loading configuration file',
                            nb_pbs)
            raise configparser.Error('Configuration error(s)')
        return nb_pbs

    def write(self, fichier):
        """
        Write a new configuration file feeded by class attributes content.

        Args:
            fichier (str) : path to configuration fileto write

        TODO: end development !
        """
        __cfg = configparser.RawConfigParser()
        for i in ['entree', 'preprocess', 'process', 'dirs']:
            __cfg.add_section(i)
            print(getattr(self, i))
            for j in getattr(self, i):
                __cfg.set(i, j, 'ok')
        with open(fichier, 'w') as __file:
            __cfg.write(__file)

    def __str__(self):

        str_msg = '<a href="https://bitbucket.org/thomas_a/maplearn">![logo]'
        str_msg += '(%s){: .logo}</a> \n\n#%s#\n' % (os.path.join(dir_app,
                'maplearn', 'app', 'img', 'logo.png'), self.metadata['title'])
        str_msg += '\n\n##Resume##'
        str_msg += '\n* Entree(s)\n'
        if not self.entree['echantillons'] is None:
            for i in self.entree['echantillons']:
                str_msg += '\n\t- Echantillons : %s' % i
        if not self.entree['data'] is None:
            str_msg += '\n\t- Donnees : %s' % self.entree['data']
        str_msg += '\n\n* Pretraitement'
        for i in ['scale', 'balance', 'separabilite']:
            if self.preprocess[i]:
                str_msg += '\n\t- %s' % self.__METADATA['preprocess'][i]
        if self.preprocess['reduction'] is not None:
            str_msg += '\n\t- Reduction des dimensions par %s' \
                      % self.preprocess['reduction']

        str_msg += '\n* Traitement : %s' % self.process['type']
        if self.process['algorithm'] is None:
            str_msg += '\n\t- Algorithmes : Tous'
        else:
            str_msg += '\n\t- %i algorithme(s) : %s' \
                   % (len(self.process['algorithm']),
                   ', '.join(self.process['algorithm']))
        if self.process['optimisation']:
            str_msg += '\n\t- Optimisation des algorithmes'
        str_msg += '\n\t- Approche %i-fold' % self.process['kfold']
        str_msg += '\n\t- Distance : %s' % self.process['distance']
        str_msg += '\n* Sortie\n\t- Dossier : %s' % self.dirs['sortie']
        return str_msg
