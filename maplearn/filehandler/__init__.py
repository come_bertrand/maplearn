# -*- coding: utf-8 -*-
"""
File handlers: read/write different format of files

* FileHandler: abstract class to handle files
* Excel
* Shapefile: geographical vector
* ImageGeo: geographical raster

"""
