# -*- coding: utf-8 -*-
"""
Mapping Learning : Unitary tests
Created on Tue Aug 16 18:37:27 2016

@author: thomas_a
"""
import os
import inspect

# application path
DIR_APP = os.path.dirname(os.path.dirname(os.path.abspath(
    inspect.getfile(inspect.currentframe()))))
DIR_DATA = os.path.join(DIR_APP, 'datasets')
DIR_TMP = os.path.join(DIR_APP, 'tmp')

if not os.path.exists(DIR_TMP):
    os.mkdir(DIR_TMP)
