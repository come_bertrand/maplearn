# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 22:00:54 2015

@author: thomas_a
"""
from __future__ import print_function

import os
import unittest

try:
    import configparser
except ImportError:
    import ConfigParser as configparser

from test import DIR_APP
from maplearn.app.config import Config


class TestConfig(unittest.TestCase):
    """ Tests unitaires de la classe Config
    """

    def setUp(self):
        self._cfg = Config(os.path.join(DIR_APP, 'examples', 'example1.cfg'))
        self._cfg.read()

    def test_missing_file(self):
        """ chargement d'une configuration à partir d'un fichier absent"""
        self.assertRaises(configparser.Error, Config, 'missing_file.cfg')

    def test_check(self):
        """
        Vérifie la configuration du fichier de configuration actuelle
        """
        self.assertEqual(self._cfg.check(), 0)

    def test_check2(self):
        """
        Vérifie la configuration d'un fichier de configuration
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example2.cfg'))
        self.assertEqual(__cfg.check(), 0)

    def test_get_legend(self):
        """
        Obtenir une legende (code/libelle des classes) à partir d'un fichier
        de configuration (avec une nomenclature contenant 7 codes)
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example2.cfg'))
        self.assertEqual(len(__cfg.entree['codes']), 7)

    def test_write(self):
        """
        vérifie ecriture fichier de config
        """
        __cfg = Config(os.path.join(DIR_APP, 'examples', 'example2.cfg'))
        fichier = os.path.join(DIR_APP, 'tmp', 'cfg_write.cfg')
        __cfg.write(fichier)
        self.assertTrue(os.path.exists(fichier))

if __name__ == '__main__':
    unittest.main()
