# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:22:42 2016

@author: thomas_a
"""
import os
import unittest
#import numpy as np
import random

from test import DIR_TMP
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.packdata import PackData
#from maplearn.filehandler.imagegeo import ImageGeo
#from maplearn.filehandler.shapefile import Shapefile
from maplearn.ml.regression import Regression


class TestRegression(unittest.TestCase):
    """ Unit tests about regression
    """

    def setUp(self):
        loader = Loader('boston')
        for _file in os.listdir(DIR_TMP):
            os.remove(os.path.join(DIR_TMP, _file))
        self.__data = PackData(X=loader.X, Y=loader.Y, data=loader.aData)
        lst_algos = ['lm', ]
        self.__algo = random.choice(lst_algos)

    def test_unknown_algo(self):
        """
        Essaie d'utiliser une classification non disponible
        """
        self.assertRaises(KeyError, Regression, self.__data, 'inexistant')

    def test_unknowns_algorithms(self):
        """
        Essaie d'utiliser une classification non disponible
        """
        self.assertRaises(KeyError, Regression, self.__data,
                          algorithms=['lm', 'inexistant', 'nimporte'])

    def test_fit(self):
        """ Si l'entrainement se passe bien => _fitted = True
        """
        reg = Regression(data=self.__data, dirOut=DIR_TMP)
        reg.fit_1(self.__algo)
        self.assertTrue(reg._fitted)

    def test_predict(self):
        """ Prediction par regression
        """
        reg = Regression(data=self.__data, algorithms=self.__algo,
                             dirOut=DIR_TMP)
        try:
            reg.run(False)
        except Exception as ex:
            print(self.__data)
            self.fail(ex)

    def test_algorithms(self):
        """ Prediction par regression
        """
        reg = Regression(data=self.__data,
                             algorithms=['lm', ],
                             dirOut=DIR_TMP)
        for algo in reg.algorithms:
            try:
                reg.predict_1(algo)
            except Exception as ex:
                self.fail("Echec algo : %s\n%s" % (algo, ex))

    def test_nn(self):
        """ Prediction using neural network
        """
        reg = Regression(data=self.__data,
                             algorithms=['mlp', ],
                             dirOut=DIR_TMP)
        for algo in reg.algorithms:
            try:
                reg.predict_1(algo)
            except Exception as ex:
                self.fail("Echec algo : %s\n%s" % (algo, ex))


if __name__ == '__main__':
    unittest.main()
