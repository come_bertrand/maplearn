# -*- coding: utf-8 -*-
"""
Created on Wed Aug 17 22:22:42 2016

@author: thomas_a
"""
import os
import unittest
import numpy as np
import random

from test import DIR_DATA, DIR_TMP
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.packdata import PackData
from maplearn.filehandler.imagegeo import ImageGeo
from maplearn.filehandler.shapefile import Shapefile
from maplearn.ml.classification import Classification


class TestClassification(unittest.TestCase):
    """ Tests unitaires de la classe Classification
    """

    def setUp(self):
        loader = Loader('iris')
        for f in os.listdir(DIR_TMP):
            os.remove(os.path.join(DIR_TMP, f))
        self.__data = PackData(X=loader.X, Y=loader.Y, data=loader.aData)
        lst_algos = ['knn', 'lda', 'rforest']
        self.__algo = random.choice(lst_algos)

    def test_unknown_classifier(self):
        """
        Essaie d'utiliser une classification non disponible
        """
        self.assertRaises(KeyError, Classification, self.__data, 'inexistant')

    def test_unknowns_algorithms(self):
        """
        Essaie d'utiliser une classification non disponible
        """
        self.assertRaises(KeyError, Classification, self.__data,
                          algorithms=['lda', 'inexistant', 'nimporte'])

    def test_fit(self):
        """ Si l'entrainement se passe bien => _fitted = True
        """
        clf = Classification(data=self.__data, dirOut=DIR_TMP,
                             algorithms='lda')
        clf.fit_1('lda')
        self.assertTrue(clf._fitted)

    def test_predict(self):
        """ Prediction par classification supervisee
        """
        clf = Classification(data=self.__data, algorithms=self.__algo,
                             dirOut=DIR_TMP)
        try:
            clf.run(False)
        except Exception as e:
            print(self.__data)
            self.fail(e)

    def test_algorithms(self):
        """ Prediction par classification
        """
        clf = Classification(data=self.__data,
                             algorithms=['knn', 'lda', 'rforest'],
                             dirOut=DIR_TMP)
        print(clf.dir_out)
        for algo in clf.algorithms:
            try:
                clf.predict_1(algo)
            except Exception as e:
                self.fail("Echec algo : %s\n%s" % (algo, e))

    def test_image_ss_ech(self):
        """
        Essai de classer une image avec une classification supervisée
        mais sans donner d'échantillon
        """
        src = os.path.join(DIR_DATA, 'landsat_rennes.tif')
        img = ImageGeo(src)
        img.read()
        data = PackData(data=img.img_2_data())
        self.assertRaises(AttributeError, Classification, data=data,
                          algorithms=self.__algo, dirOut=DIR_TMP)

    def test_image(self):
        """
        Classification supervisée appliquée à une image à partir d'échantillons
        chargés à partir d'une autre image
        """
        img = ImageGeo(os.path.join(DIR_DATA, 'landsat_rennes.tif'))
        img.read()
        samples = ImageGeo(os.path.join(DIR_DATA,
                                        'samples_landsat_rennes.tif'))
        samples.read()
        y = samples.data[samples.data > 0]
        x = img.data[samples.data > 0]
        data = PackData(X=x, Y=y, data=img.img_2_data(),
                codes={'urbain': 1, 'sol nu': 2, 'bois': 3, 'veg. basse': 4})
        clf = Classification(data=data, algorithms=self.__algo,
                             dirOut=DIR_TMP)
        clf.run(True)
        img.data_2_img(clf.result, True)
        out_file = os.path.join(DIR_TMP, 'classif.tif')
        img.write(out_file)
        # vérifie que l'image existe et qu'il s'agit bien d'une matrice entière
        self.assertTrue(os.path.exists(out_file))

        img = ImageGeo(out_file)
        img.read()

        # vérifie que l'image est bien une image avec des entiers, avec le
        # nombre de classes attendues
        self.assertLessEqual(len(np.unique(img.data)), 4)

    def test_shapefile(self):
        """
        Classification supervisée appliquée à un shapefile
        """
        in_file = os.path.join(DIR_DATA, 'echantillon.shp')
        shp = Loader(in_file, features=['Brightness', ], classe='ECH')
        shp.run()

        data = PackData(X=shp.X, Y=shp.Y, data=shp.aData)
        clf = Classification(data=data, algorithms=self.__algo,
                             dirOut=DIR_TMP)
        clf.run(True)
        out_file = os.path.join(DIR_TMP, 'classif.shp')
        shp = Shapefile(in_file)
        shp.read()
        shp.write(out_file, clf.result)
        # vérifie que l'image existe et qu'il s'agit bien d'une matrice entière
        self.assertTrue(os.path.exists(out_file))

        shp = Shapefile(out_file)
        shp.read()

        # verifie le nombre de classes dans la classif
        self.assertLessEqual(len(np.unique(shp.data)), 3)

    def test_nn(self):
        """
        Classification using neural network
        """
        clf = Classification(data=self.__data,
                             algorithms=['mlp'],
                             dirOut=DIR_TMP)
        print(clf.dir_out)
        for algo in clf.algorithms:
            try:
                clf.predict_1(algo)
            except Exception as e:
                self.fail("Echec algo : %s\n%s" % (algo, e))

if __name__ == '__main__':
    unittest.main()
