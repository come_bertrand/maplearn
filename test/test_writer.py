# -*- coding: utf-8 -*-
"""
Created on Tue Aug 16 18:57:34 2016

@author: thomas_a
"""
import os
import unittest
import random

from test import DIR_DATA, DIR_TMP
from maplearn.datahandler.loader import Loader
from maplearn.datahandler.writer import Writer
from maplearn.filehandler.shapefile import Shapefile
from maplearn.filehandler.imagegeo import ImageGeo
from maplearn.filehandler.shapefile import Shapefile


class TestWriter(unittest.TestCase):
    """
    Unit tests about writing data into a file
    """

    def test_fichier_inexistant(self):
        """
        Writes in a non-existent file
        """
        self.assertRaises(IOError, Writer, '/fichier/inexistant')

    def test_existing_file(self):
        """
        Access to an already existing file
        """
        str_file = os.path.join(DIR_DATA, 'ex1.xlsx')
        try:
            Writer(str_file)
        except:
            self.fail('Unable to get access to %s' % str_file)

    def test_write_dataset(self):
        """
        Loads a known dataset (included in Scikit-learn) and writes it into an
        excel file.
        """
        data = random.choice(['boston', 'iris', 'digits'])
        loader = Loader(data)
        out_file = os.path.join(DIR_TMP, 'dataset.xls')
        writer = Writer(None)
        writer.run(data=loader.aData, fichier=out_file)
        
        self.assertTrue(os.path.exists(out_file))

if __name__ == '__main__':
    unittest.main()
